﻿//Copyright © 2017-2018 Soluciones Digitales
//Todos los derechos reservados

using Logger;
using Newtonsoft.Json;
using System;
using System.Deployment.Application;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace ReadFast.Controladores
{
    internal class Nucleo
    {
        //Se asigna la direccion donde se guardaran los ajustes
        private readonly string _ajustesDir = Application.StartupPath + @"\Ajustes\Ajustes.json";

        //Objeto que contiene los metodos de la clase Logger
        ToLog log = new ToLog();

        /// <summary>
        /// Crea el directorio y archivo que permite guardar los ajustes en formato json
        /// </summary>
        /// <param name="ajustes"></param>
        /// <returns>Retorna un booleano dependiendo si la accion se ejecuto con exito</returns>
        public bool GuardarAjustes(Configuraciones ajustes)
        {
            try
            {
                //Verifica que exista el directorio "Ajustes"
                if (!Directory.Exists(Application.StartupPath + @"\Ajustes"))
                {
                    //Crea el directorio si, no existe
                    Directory.CreateDirectory(Application.StartupPath + @"\Ajustes");
                    //Guarda el evento en el log
                    log.AppendLog("Se creo el directorio Ajustes");
                }

                //Verifica si existe el archivo json
                if (!File.Exists(_ajustesDir))
                {
                    //Crea el archivo json
                    using (File.Create(_ajustesDir)) { }
                    //Guarda el evento en el log
                    log.AppendLog("Se creo el archivo Ajustes.json");
                }

                //Convierte la estructura en formato json
                string datos = JsonConvert.SerializeObject(ajustes, Formatting.Indented);

                //Guarda los datos de los ajustes en un archivo json
                File.WriteAllText(_ajustesDir, datos);

                //Guarda la accion en el log
                log.AppendLog("Se guardaron los ajustes correctamente");

                //Retorna true cuando se crea todo correctamente
                return true;
            }
            catch (Exception ex)
            {
                //Guarda en el log cualquier error producido
                log.AppendLog(ex.Message);
                //Retorna false al no poder guardar los ajustes
                return false;
            }
        }

        /// <summary>
        /// Obtiene los ajustes del archivo json
        /// </summary>
        /// <returns></returns>
        public Configuraciones LeerAjustes()
        {
            try
            {
                //Verifica que exista el archivo de ajustes
                if (!File.Exists(_ajustesDir))
                {
                    //Crea el archivo json
                    using (File.Create(_ajustesDir)) { }
                    //Guarda en el log el evento
                    log.AppendLog("Imposible obtener los datos debido a que el archivo no existia");
                    //Retorna configuraciones vacias
                    return null;
                }

                else
                {
                    //Lee los datos del archivo json
                    string datos = File.ReadAllText(_ajustesDir);
                    //Convierte los datos en una estructura reconocible
                    Configuraciones ajustes = JsonConvert.DeserializeObject<Configuraciones>(datos);
                    //Guarda en el log el evento
                    log.AppendLog("Ajustes leídos correctamente");
                    //Devuelve los ajustes leidos
                    return ajustes;
                }
            }
            catch (Exception ex)
            {
                //Guarda en el log cualquier error producido
                log.AppendLog(ex.Message);
                //Retorna null al no poder leer los ajustes
                return null;
            }
        }

        /// <summary>
        /// Obtiene la version actual del programa
        /// </summary>
        public string ObtenerVersion
        {
            get
            {
                return ApplicationDeployment.IsNetworkDeployed
                       ? ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString()
                       : Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }
    }
}
