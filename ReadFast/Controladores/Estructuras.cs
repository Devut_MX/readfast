﻿//Copyright © 2017-2018 Soluciones Digitales
//Todos los derechos reservados

namespace ReadFast.Controladores
{
    //Todas las estructuras aqui establecidas se usan para minorizar las variables
    public class Configuraciones
    {
        public string Aplicacion { get; set; }
        public string Datos { get; set; }
        public string Perfil { get; set; }
        public string Servidor { get; set; }
        //public string ServidorImagen { get; set; }
        public string FormatoImagen { get; set; }
    }

    class Propiedades
    {
        //public string aplicacion { get; set; }
        //public string perfil { get; set; }
        public string escaner { get; set; }
        public bool PermiteImagen { get; set; }
    }

    public class AJSON
    {
        public string session { get; set; }
        public string scanner { get; set; }
        public string application { get; set; }
        public string profile { get; set; }
        public bool imagecapable { get; set; }
        public string datfile { get; set; }
        public string maindirectory { get; set; }
    }
}
