﻿namespace ReadFast.Vistas
{
    partial class Envio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Envio));
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.bsiVersion = new DevExpress.XtraBars.BarStaticItem();
            this.bsiSolucionesDigitales = new DevExpress.XtraBars.BarStaticItem();
            this.btnIniciarEnvio = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.btnDirJSON = new DevExpress.XtraEditors.SimpleButton();
            this.txtDirJSON = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lblEstatus = new DevExpress.XtraEditors.LabelControl();
            this.SSMProcesando = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::ReadFast.Vistas.Procesando), true, true);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDirJSON.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.bsiVersion,
            this.bsiSolucionesDigitales,
            this.btnIniciarEnvio});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ribbon.MaxItemId = 5;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbon.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.MacOffice;
            this.ribbon.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.ShowOnMultiplePages;
            this.ribbon.ShowToolbarCustomizeItem = false;
            this.ribbon.Size = new System.Drawing.Size(455, 134);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            this.ribbon.Toolbar.ShowCustomizeItem = false;
            // 
            // bsiVersion
            // 
            this.bsiVersion.Caption = "Versión 1.0.0.0";
            this.bsiVersion.Id = 2;
            this.bsiVersion.Name = "bsiVersion";
            // 
            // bsiSolucionesDigitales
            // 
            this.bsiSolucionesDigitales.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bsiSolucionesDigitales.Caption = "2017 © Soluciones Digitales";
            this.bsiSolucionesDigitales.Id = 3;
            this.bsiSolucionesDigitales.Name = "bsiSolucionesDigitales";
            this.bsiSolucionesDigitales.ItemDoubleClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bsiSolucionesDigitales_ItemDoubleClick);
            // 
            // btnIniciarEnvio
            // 
            this.btnIniciarEnvio.Caption = "Iniciar envío";
            this.btnIniciarEnvio.Id = 4;
            this.btnIniciarEnvio.ImageOptions.LargeImage = global::ReadFast.Properties.Resources.Enviar;
            this.btnIniciarEnvio.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I));
            this.btnIniciarEnvio.Name = "btnIniciarEnvio";
            this.btnIniciarEnvio.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnIniciarEnvio_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "Opciones";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.btnIniciarEnvio);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "Opciones de envío";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.bsiVersion);
            this.ribbonStatusBar.ItemLinks.Add(this.bsiSolucionesDigitales);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 274);
            this.ribbonStatusBar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(455, 40);
            // 
            // btnDirJSON
            // 
            this.btnDirJSON.Location = new System.Drawing.Point(349, 180);
            this.btnDirJSON.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnDirJSON.Name = "btnDirJSON";
            this.btnDirJSON.Size = new System.Drawing.Size(87, 28);
            this.btnDirJSON.TabIndex = 17;
            this.btnDirJSON.Text = "Seleccionar...";
            this.btnDirJSON.Click += new System.EventHandler(this.btnDirJSON_Click);
            // 
            // txtDirJSON
            // 
            this.txtDirJSON.Location = new System.Drawing.Point(14, 183);
            this.txtDirJSON.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtDirJSON.MenuManager = this.ribbon;
            this.txtDirJSON.Name = "txtDirJSON";
            this.txtDirJSON.Properties.ReadOnly = true;
            this.txtDirJSON.Size = new System.Drawing.Size(328, 22);
            this.txtDirJSON.TabIndex = 16;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(14, 160);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(265, 16);
            this.labelControl2.TabIndex = 15;
            this.labelControl2.Text = "Indique el archivo con la información a envíar:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(14, 236);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(46, 16);
            this.labelControl1.TabIndex = 18;
            this.labelControl1.Text = "Estatus:";
            // 
            // lblEstatus
            // 
            this.lblEstatus.Location = new System.Drawing.Point(68, 236);
            this.lblEstatus.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblEstatus.Name = "lblEstatus";
            this.lblEstatus.Size = new System.Drawing.Size(15, 16);
            this.lblEstatus.TabIndex = 19;
            this.lblEstatus.Text = "---";
            // 
            // SSMProcesando
            // 
            this.SSMProcesando.ClosingDelay = 500;
            // 
            // Envio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(455, 314);
            this.Controls.Add(this.lblEstatus);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.btnDirJSON);
            this.Controls.Add(this.txtDirJSON);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "Envio";
            this.Ribbon = this.ribbon;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "ReadFast | Envío de datos";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Envio_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDirJSON.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.BarStaticItem bsiVersion;
        private DevExpress.XtraBars.BarStaticItem bsiSolucionesDigitales;
        private DevExpress.XtraBars.BarButtonItem btnIniciarEnvio;
        private DevExpress.XtraEditors.SimpleButton btnDirJSON;
        private DevExpress.XtraEditors.TextEdit txtDirJSON;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl lblEstatus;
        private DevExpress.XtraSplashScreen.SplashScreenManager SSMProcesando;
    }
}