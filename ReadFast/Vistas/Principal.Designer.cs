﻿namespace ReadFast.Vistas
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Principal));
            this.SSMProcesando = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::ReadFast.Vistas.Procesando), true, true);
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.bsiVersion = new DevExpress.XtraBars.BarStaticItem();
            this.bsiSolucionesDigitales = new DevExpress.XtraBars.BarStaticItem();
            this.btnMenuCerrar = new DevExpress.XtraBars.BarButtonItem();
            this.btnMenuIniciarEscaneo = new DevExpress.XtraBars.BarButtonItem();
            this.btnMenuTerminarEscaneo = new DevExpress.XtraBars.BarButtonItem();
            this.btnMenuAjustes = new DevExpress.XtraBars.BarButtonItem();
            this.btnMenuAyuda = new DevExpress.XtraBars.BarButtonItem();
            this.btnMenuEnvioDatos = new DevExpress.XtraBars.BarButtonItem();
            this.btnMenuRefrescar = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.lbcLista = new DevExpress.XtraEditors.ListBoxControl();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbcLista)).BeginInit();
            this.SuspendLayout();
            // 
            // SSMProcesando
            // 
            this.SSMProcesando.ClosingDelay = 500;
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.bsiVersion,
            this.bsiSolucionesDigitales,
            this.btnMenuCerrar,
            this.btnMenuIniciarEscaneo,
            this.btnMenuTerminarEscaneo,
            this.btnMenuAjustes,
            this.btnMenuAyuda,
            this.btnMenuEnvioDatos,
            this.btnMenuRefrescar});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ribbon.MaxItemId = 13;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbon.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.MacOffice;
            this.ribbon.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.ShowOnMultiplePages;
            this.ribbon.ShowToolbarCustomizeItem = false;
            this.ribbon.Size = new System.Drawing.Size(588, 134);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            this.ribbon.Toolbar.ShowCustomizeItem = false;
            // 
            // bsiVersion
            // 
            this.bsiVersion.Caption = "Versión 1.0.0.0";
            this.bsiVersion.Id = 1;
            this.bsiVersion.Name = "bsiVersion";
            // 
            // bsiSolucionesDigitales
            // 
            this.bsiSolucionesDigitales.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bsiSolucionesDigitales.Caption = "2017 © Soluciones Digitales";
            this.bsiSolucionesDigitales.Id = 2;
            this.bsiSolucionesDigitales.Name = "bsiSolucionesDigitales";
            this.bsiSolucionesDigitales.ItemDoubleClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bsiSolucionesDigitales_ItemDoubleClick);
            // 
            // btnMenuCerrar
            // 
            this.btnMenuCerrar.Caption = "Cerrar";
            this.btnMenuCerrar.Id = 3;
            this.btnMenuCerrar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnMenuCerrar.ImageOptions.Image")));
            this.btnMenuCerrar.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnMenuCerrar.ImageOptions.LargeImage")));
            this.btnMenuCerrar.Name = "btnMenuCerrar";
            // 
            // btnMenuIniciarEscaneo
            // 
            this.btnMenuIniciarEscaneo.Caption = "Iniciar Escaneo";
            this.btnMenuIniciarEscaneo.Enabled = false;
            this.btnMenuIniciarEscaneo.Id = 5;
            this.btnMenuIniciarEscaneo.ImageOptions.LargeImage = global::ReadFast.Properties.Resources.Escaner200;
            this.btnMenuIniciarEscaneo.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I));
            this.btnMenuIniciarEscaneo.Name = "btnMenuIniciarEscaneo";
            this.btnMenuIniciarEscaneo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnMenuIniciarEscaneo_ItemClick);
            // 
            // btnMenuTerminarEscaneo
            // 
            this.btnMenuTerminarEscaneo.Caption = "Terminar Escaneo";
            this.btnMenuTerminarEscaneo.Id = 6;
            this.btnMenuTerminarEscaneo.ImageOptions.LargeImage = global::ReadFast.Properties.Resources.Detener200;
            this.btnMenuTerminarEscaneo.Name = "btnMenuTerminarEscaneo";
            this.btnMenuTerminarEscaneo.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // btnMenuAjustes
            // 
            this.btnMenuAjustes.Caption = "Ajustes";
            this.btnMenuAjustes.Id = 7;
            this.btnMenuAjustes.ImageOptions.LargeImage = global::ReadFast.Properties.Resources.Ajustes200;
            this.btnMenuAjustes.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F2);
            this.btnMenuAjustes.Name = "btnMenuAjustes";
            this.btnMenuAjustes.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnMenuAjustes_ItemClick);
            // 
            // btnMenuAyuda
            // 
            this.btnMenuAyuda.Caption = "Ayuda";
            this.btnMenuAyuda.Id = 8;
            this.btnMenuAyuda.ImageOptions.LargeImage = global::ReadFast.Properties.Resources.Ayuda200;
            this.btnMenuAyuda.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F1);
            this.btnMenuAyuda.Name = "btnMenuAyuda";
            this.btnMenuAyuda.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // btnMenuEnvioDatos
            // 
            this.btnMenuEnvioDatos.Caption = "Enviar datos";
            this.btnMenuEnvioDatos.Id = 10;
            this.btnMenuEnvioDatos.ImageOptions.Image = global::ReadFast.Properties.Resources.Enviar;
            this.btnMenuEnvioDatos.ImageOptions.LargeImage = global::ReadFast.Properties.Resources.Enviar;
            this.btnMenuEnvioDatos.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E));
            this.btnMenuEnvioDatos.Name = "btnMenuEnvioDatos";
            this.btnMenuEnvioDatos.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnMenuEnvioDatos_ItemClick);
            // 
            // btnMenuRefrescar
            // 
            this.btnMenuRefrescar.Caption = "Refrescar dispositivos";
            this.btnMenuRefrescar.Id = 12;
            this.btnMenuRefrescar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnMenuRefrescar.ImageOptions.Image")));
            this.btnMenuRefrescar.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnMenuRefrescar.ImageOptions.LargeImage")));
            this.btnMenuRefrescar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F5);
            this.btnMenuRefrescar.Name = "btnMenuRefrescar";
            this.btnMenuRefrescar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnMenuRefrescar_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "Opciones principales";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.btnMenuIniciarEscaneo);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnMenuEnvioDatos);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnMenuRefrescar);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnMenuAjustes);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnMenuAyuda);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "Opciones principales";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.bsiVersion);
            this.ribbonStatusBar.ItemLinks.Add(this.bsiSolucionesDigitales);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 439);
            this.ribbonStatusBar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(588, 40);
            // 
            // lbcLista
            // 
            this.lbcLista.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbcLista.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbcLista.Location = new System.Drawing.Point(0, 134);
            this.lbcLista.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lbcLista.Name = "lbcLista";
            this.lbcLista.Size = new System.Drawing.Size(588, 305);
            this.lbcLista.TabIndex = 2;
            // 
            // Principal
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(588, 479);
            this.Controls.Add(this.lbcLista);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "Principal";
            this.Ribbon = this.ribbon;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "ReadFast";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Principal_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Principal_FormClosed);
            this.Shown += new System.EventHandler(this.Principal_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbcLista)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.BarStaticItem bsiVersion;
        private DevExpress.XtraBars.BarStaticItem bsiSolucionesDigitales;
        private DevExpress.XtraBars.BarButtonItem btnMenuCerrar;
        private DevExpress.XtraBars.BarButtonItem btnMenuIniciarEscaneo;
        private DevExpress.XtraBars.BarButtonItem btnMenuTerminarEscaneo;
        private DevExpress.XtraBars.BarButtonItem btnMenuAjustes;
        private DevExpress.XtraBars.BarButtonItem btnMenuAyuda;
        private DevExpress.XtraEditors.ListBoxControl lbcLista;
        private DevExpress.XtraSplashScreen.SplashScreenManager SSMProcesando;
        private DevExpress.XtraBars.BarButtonItem btnMenuEnvioDatos;
        private DevExpress.XtraBars.BarButtonItem btnMenuRefrescar;
    }
}