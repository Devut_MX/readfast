﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraSplashScreen;

namespace ReadFast.Vistas
{
    public partial class ReadFastSplash : SplashScreen
    {
        public ReadFastSplash()
        {
            InitializeComponent();
        }

        #region Overrides

        public override void ProcessCommand(Enum cmd, object arg)
        {
            base.ProcessCommand(cmd, arg);
        }

        #endregion

        int segundos = 0;

        public enum SplashScreenCommand
        {
        }

        private void tmpSplash_Tick(object sender, EventArgs e)
        {
            if (segundos >= 2)
            {
                tmpSplash.Enabled = false;

                Principal mostrar = Principal.ObtenerInstancia();
                
                Hide();

                mostrar.ShowDialog();
            }

            else
            {
                segundos++;
            }
        }
    }
}