﻿//Copyright © 2017-2018 Soluciones Digitales
//Todos los derechos reservados

using DevExpress.LookAndFeel;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using Logger;
using Microsoft.Win32;
using Newtonsoft.Json;
using ReadFast.Controladores;
using ScanToolsPlusLink;
using System;
using System.Data.OleDb;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace ReadFast.Vistas
{
    public partial class Principal : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        private static Principal singleton;

        /// <summary>
        /// Detecta si hay una instancia actual de la ventana para evitar mostrarla mas de una vez
        /// </summary>
        /// <returns>Devuelve la instancia</returns>
        public static Principal ObtenerInstancia()
        {
            if (singleton == null || singleton.IsDisposed)
            {
                singleton = new Principal();
            }

            singleton.BringToFront();

            return singleton;
        }

        private Principal()
        {
            InitializeComponent();

            log.RestoreLog();

            VerificarDirectorios();
        }
        
        //Objeto que contiene los metodos de la clase nucleo
        Nucleo _nucleo = new Nucleo();

        //Objeto que contiene los metodos de la clase Logger
        ToLog log = new ToLog();

        //Variable con estructura para guardarse en json
        Propiedades _propiedades = null;

        /// <summary>
        /// Detecta y crea los directorios necesarios para el funcionamiento de ReadFast
        /// </summary>
        private void VerificarDirectorios()
        {
            try
            {
                if (!Directory.Exists(Application.StartupPath + @"\Aplicacion"))
                {
                    Directory.CreateDirectory(Application.StartupPath + @"\Aplicacion");
                }

                if (!Directory.Exists(Application.StartupPath + @"\Datos"))
                {
                    Directory.CreateDirectory(Application.StartupPath + @"\Datos");
                }

                if (!Directory.Exists(Application.StartupPath + @"\Perfil"))
                {
                    Directory.CreateDirectory(Application.StartupPath + @"\Perfil");
                }

                log.AppendLog("Directorios creados satisfactoriamente");
            }
            catch (Exception ex)
            {
                log.AppendLog(ex.Message);
            }
        }

        /// <summary>
        /// Inicializa la configuracion y comunicacion con el escaner
        /// </summary>
        private void Inicializar()
        {
            try
            {
                CreateScanningManager(lbcLista);
                log.AppendLog("Se creo el manejador del escaner");
                SetupScanner(_scanningManager, lbcLista);
                log.AppendLog("Se creo/restablecio las configuraciones del escaner");

                btnMenuIniciarEscaneo.Enabled = true;
            }
            catch (Exception ex)
            {
                log.AppendLog(ex.Message);
                log.AppendLog("Fallo el arranque del escaner, verifique las conexiones o los ajustes");
                lbcLista.Items.Add("Fallo el arranque del escaner, verifique las conexiones o los ajustes");
            }
        }
        
        /// <summary>
        /// Al mostrarse la pantalla principal de ReadFast se ejecuta la inicializacion del escaner
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Principal_Shown(object sender, EventArgs e)
        {
            try
            {
                Inicializar();

                bsiVersion.Caption = "Versión " + _nucleo.ObtenerVersion;

                log.AppendLog("Arranque de ReadFast correcto --------------------------------------------");
            }
            catch (Exception ex)
            {
                //Guarda en el log cualquier error producido
                log.AppendLog(ex.Message);
            }
        }

        #region Declaracion de variables
        
        //Variables requeridas por el SDK del escaner
        bool RequireUserToConfirmOptions = true;
        bool ScannerInfoChanged = false;

        L_PORT_ENUM Port = L_PORT_ENUM.L_PORT_USE_CURRENT;
        bool DisableBarcodeReader = false;
        bool DisableTransportPrinter = false;
        bool HasSelectStacker = false;
        L_SCANNERMODEL_ENUM ScannerModel = 0;
        L_PORT_ENUM BarcodePort = 0;
        L_BAUDRATE_ENUM BaudRate = 0;
        L_DATABITS_ENUM Databits = 0;
        L_PARITY_ENUM Parity = 0;
        L_STOPBITS_ENUM Stopbits = 0;
        short Timeout = 0;
        string DisplayName = "";
        int WriteOperationsLog = 0;

        //Variables necesarias para el procesamiento de los datos
        private int Conteo = 0; //Mantiene en serie el conteo de hojas escaneadas
        private int serie = 1; //Mantiene en serie el numero de imagenes guardadas por el escaner
        private string identificador = null; //Guarda el valor del identificador de cada hoja leida
        private string FolioSesion = null; //Guarda el numero de Folio para la sesion
        private string directorioPrincipal = null; //Guarda el directorio donde se almacenan los datos
        private string directorioHijo = null; //Guarda el directorio donde se almacenan los escaneos
        private string directorioImagenes = null; //Guarda el directorio donde se almacenan las imagenes del escaneo
        private string archivoDAT = null; //Guarda el directorio donde se guarda el archivo .dat del escaneo

        #endregion

        #region Crea y desecha la informacion del escaneo SessionInfo

        //Variable de informacion de la sesion de escaneo (Variable de SDK)
        SessionInfo _sessionInfo;
        
        /// <summary>
        /// Genera la configuracion para la sesion del escaneo
        /// </summary>
        public void CreateSessionInfo()
        {
            try
            {
                if (_sessionInfo == null)
                {
                    //Se inicializa la variable de sesion
                    _sessionInfo = new SessionInfo();

                    //Se le asigna un folio de escaneo (Irrepetible) conformado por la palabra "Escaneo-" + año + mes + dia + hora + min + seg 
                    FolioSesion = "Escaneo-" + DateTime.Now.ToString("yyMMddHHmmss");

                    //Asigna a la variable el nombre del directorio dentro de la carpeta que indique para su guardado mas el nombre del folio
                    directorioPrincipal = _nucleo.LeerAjustes().Datos + "\\" + FolioSesion;
                    
                    //Verifica si existe el directorio donde se almacenaran los datos del escaneo (Este bloque if se ejecuta cuando no existe el directorio)
                    if (!Directory.Exists(directorioPrincipal))
                    {
                        //Crea el directorio con el folio del escaneo
                        Directory.CreateDirectory(directorioPrincipal);

                        //Asigna a la variable el directorio dentro del directorio principal con nombre "Datos"
                        directorioHijo = directorioPrincipal + @"\Datos";

                        //Crea el directorio hijo "Datos" dentro del directorio principal
                        Directory.CreateDirectory(directorioHijo);

                        //Asigna a la variable el directorio "Evidencias" dentro del directorio "Datos"
                        directorioImagenes = directorioHijo + @"\Evidencias";

                        //Crea el directorio "Evidencias" dentro del directorio "Datos"
                        Directory.CreateDirectory(directorioImagenes);

                        //Graba el texto en el log cuando termina de crear directorios
                        log.AppendLog("Directorios de estructura creados");
                    }

                    //Asigna a la variable la direccion y formato del archivo que contendra la informacion obtenida del escaneo
                    archivoDAT = directorioHijo + @"\Valores.dat";
                    
                    //Lee las configuraciones de la pantalla ajustes para determinar el archivo .StxAppDef que indico el usuario (Variable de SDK)
                    _sessionInfo.Application = _nucleo.LeerAjustes().Aplicacion;

                    //Asigna el directorio y archivo donde se guardarán los datos obtenidos de la sesion de escaneo (Variable de SDK)
                    _sessionInfo.DataFile = archivoDAT;

                    //Lee las configuraciones de la pantalla ajustes para determinar el archivo .StxCnvPrf que indico el usuario (Variable de SDK)
                    _sessionInfo.ConversionProfile = _nucleo.LeerAjustes().Perfil;

                    //Evita que al escanear con imagenes, se generen imagenes vacias(Variable de SDK)
                    _sessionInfo.TakePhantomClips = false;

                    //Garantizar siempre que se muestre la ventana de progreso de escaneo (Variable de SDK)
                    _sessionInfo.DisplayScanningProgressWindow = 1;

                    //Prepara la fuente de luz necesaria para el escaneo de marcas OMR (Variable de SDK)
                    _sessionInfo.LightSource = L_LIGHT_SOURCE_ENUM.L_LIGHT_SOURCE_IR;

                    //Detiene el escaneo despues de una determinada cantidad de escaneos seguidos, el 0 denota ilimitados, no usar la variable implica que se escanearan indeterminados documentos (Variable de SDK)
                    //_sessionInfo.StopAfter = 0;

                    //Genera un archivo con los datos obtenidos del escaneo, reemplazando al anterior si existe alguno (Variable de SDK)
                    _sessionInfo.DataFileOption = L_DATA_FILE_OPTION_ENUM.L_DATA_FILE_OPTION_REPLACE;

                    //Inicia el escaneo con la configuracion del puerto COM donde se haya conectado el escaner (Variable de SDK)
                    _scanningManager.Scan(_sessionInfo, L_PORT_ENUM.L_PORT_USE_CURRENT);

                    //Guarda en el log la notificacion de configuracion
                    log.AppendLog("Se configuro la sesion de escaneo correctamente");

                    //Muestra en pantalla que la sesion de escaneado esta lista
                    lbcLista.Items.Add("Escaneando los documentos");
                }
            }
            catch (Exception ex)
            {
                //Guarda en el log algun error producido durante la configuracion de la sesion de escaneo
                log.AppendLog(ex.Message);
            }
        }

        /// <summary>
        /// Desecha la configuracion de la sesion de escaneo
        /// </summary>
        public void DisposeSessionInfo()
        {
            try
            {
                //Verifica si hay datos en la sesion
                if (_sessionInfo != null)
                {
                    //Libera el objeto de los registros que contienen la configuracion de la sesion
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(_sessionInfo);

                    //Indica que la sesion a quedado nula
                    _sessionInfo = null;

                    //Guarda en el log la accion de borrado
                    log.AppendLog("Se vacio el objeto de con la sesion");
                }
            }
            catch (Exception ex)
            {
                //Guarda en el log cualquier error producido
                log.AppendLog(ex.Message);
            }
        }

        #endregion

        #region Crea y desecha el ScanningManager

        //Guarda los ajustes de hardware del escaner (Variable de SDK)
        private ScanningManager _scanningManager;

        /// <summary>
        /// Crea la configuracion con los eventos necesarios para preparar una sesion de escaneo
        /// </summary>
        /// <param name="lista">ListBoxControl donde se mostrarán los mensajes al usuario</param>
        public void CreateScanningManager(ListBoxControl lista)
        {
            try
            {
                //Detecta si no existe alguna otra configuracion previa
                if (_scanningManager == null)
                {
                    //Inicializa la variable de configuracion del hardware (Variable de SDK)
                    _scanningManager = new ScanningManager();

                    //Muestra al usuario la version del runtime instalado
                    lista.Items.Add("Versión del runtime: " + _scanningManager.Version);

                    //Le indica la licencia de uso necesaria para poder trabajar (Variable de SDK)
                    _scanningManager.License = "1AL39Y40L90JCQ";

                    //Muestra al usuario las funciones que le permite la licencia adquirida (No es necesaria)
                    //lista.Items.Add("Licencia: " + _scanningManager.License);

                    //Le indica a la variable de configuracion que los eventos seran generados a partir de esta ventana
                    _scanningManager.SetClientWindowHandle(Handle.ToInt32());

                    //Eventos requeridos para el correcto funcionamiento del escaner (No se utilizan todos, pero son los disponibles)
                    _scanningManager.ScannerSearch += _scanningManager_ScannerSearch;
                    _scanningManager.ScannerFound += _scanningManager_ScannerFound;
                    _scanningManager.ScannerSearchComplete += _scanningManager_ScannerSearchComplete;
                    _scanningManager.DocComplete += _scanningManager_DocComplete;
                    _scanningManager.SessionInit += _scanningManager_SessionInit;
                    _scanningManager.SessionComplete += _scanningManager_SessionComplete;
                    _scanningManager.DocInit += _scanningManager_DocInit;
                    _scanningManager.SheetInit += _scanningManager_SheetInit;
                    _scanningManager.SheetAcquired += _scanningManager_SheetAcquired;
                    _scanningManager.FormIdentified += _scanningManager_FormIdentified;
                    _scanningManager.Resolved += _scanningManager_Resolved;
                    _scanningManager.PreScore += _scanningManager_PreScore;
                    _scanningManager.XPrint += _scanningManager_XPrint;
                    _scanningManager.Clip += _scanningManager_Clip;
                    _scanningManager.SheetStacked += _scanningManager_SheetStacked;
                    _scanningManager.SheetComplete += _scanningManager_SheetComplete;
                    _scanningManager.SheetCancelled += _scanningManager_SheetCancelled;
                    _scanningManager.DocStacked += _scanningManager_DocStacked;
                    _scanningManager.DocRejected += _scanningManager_DocRejected;
                    _scanningManager.DocCancelled += _scanningManager_DocCancelled;
                    _scanningManager.PreTerminate += _scanningManager_PreTerminate;
                    _scanningManager.ScanError += _scanningManager_ScanError;
                    _scanningManager.ScannerButtonPressed += _scanningManager_ScannerButtonPressed;
                    _scanningManager.ServerMessageBox += _scanningManager_ServerMessageBox;

                    //Guarda en el log la correcta creacion de los eventos
                    log.AppendLog("Eventos para el escaneo creados satisfactoriamente");
                }
            }
            catch (Exception ex)
            {
                //Guarda en el log cualquier error producido
                log.AppendLog(ex.Message);
            }
        }

        /// <summary>
        /// Desecha toda la informacion alojada en la variable del gestor de escaneo
        /// </summary>
        public void DisposeScanningManager()
        {
            try
            {
                //Detecta si la variable no esta vacia
                if (_scanningManager != null)
                {
                    //Libera los recursos utilizados por la variable
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(_scanningManager);

                    //Guarda la informacion de vaciado en el Log
                    log.AppendLog("Se vacio el objeto de gestor de escaneo");

                    //Se nulifica el valor de la variable
                    _scanningManager = null;
                }
            }
            catch (Exception ex)
            {
                //Se guarda en el log cualquier problema presentado
                log.AppendLog(ex.Message);
            }
        }

        #endregion

        #region Busca y configura el escaner

        /// <summary>
        /// Realiza la deteccion y revisa la comunicacion del escaner
        /// </summary>
        /// <param name="scanningManager"></param>
        /// <returns>Retorna un booleano dependiendo el registro del escaner</returns>
        public bool SetupScannerRegistry(ScanningManager scanningManager)
        {
            try
            {
                //En caso de recibir respuesta del escaner
                if (!(scanningManager.ActiveScanner.State == L_SCANNER_STATE_ENUM.L_SCANNER_STATE_NORESPONSE))
                {
                    //Retorna que el escaner esta listo
                    return true;
                }

                //Contiene los datos del hardware del escaner empleado (Variable de SDK)
                HardwareConfig hw = scanningManager.ActiveScanner.HardwareConfig;

                //Variable que obtendra los datos del registro que se usan al detectar un escaner
                string myHwConfig = "";

                //Obtiene de las entradas del registro los valores del escaner
                myHwConfig = Registry.GetValue(@"HKEY_CURRENT_USER\SOFTWARE\Scantron\ScanTools Plus Link SDK\Common", "HwConfig", "").ToString();

                //Si las configuraciones no estan vacias
                if (myHwConfig != "")
                {
                    try
                    {
                        //Pasa la configuracion previa a la actual sesion
                        scanningManager.ActiveScanner.Configuration = myHwConfig;

                        //Evalua si el escaner esta configurado
                        if (scanningManager.ActiveScanner.State == L_SCANNER_STATE_ENUM.L_SCANNER_STATE_CONFIGURED)
                        {
                            //Retorna que el escaner esta listo
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        //Vacia los valores del escaner en las entradas del registro en caso de algun error
                        Registry.SetValue(@"HKEY_CURRENT_USER\SOFTWARE\Scantron\ScanTools Plus Link SDK\Common", "HwConfig", "");

                        //Guarda en el log el error obtenido
                        log.AppendLog(ex.Message);
                    }
                }

                //Si no se pudo configurar el escaner, se retorna un false
                return false;
            }
            catch (Exception ex)
            {
                //Guarda en el log el error obtenido
                log.AppendLog(ex.Message);

                //Si no se pudo configurar el escaner, se retorna un false al obtener cualquier error imprevisto
                return false;
            }
        }

        /// <summary>
        /// Configura el escaner segun el modelo
        /// </summary>
        /// <param name="scanningManager">Variable de configuracion de hardware del escaner</param>
        /// <param name="lista">ListBoxControl donde se mostrarán los mensajes al usuario</param>
        public void SetupScanner(ScanningManager scanningManager, ListBoxControl lista)
        {
            try
            {
                //Indica si la informacion del escaner a cambiado (Variable de SDK)
                ScannerInfoChanged = true;

                //Se muestra mensaje de que se esta buscando escaner
                lista.Items.Add("Detectando el escaner...");
                //Se guarda en el log el inicio de deteccion de un escaner
                log.AppendLog("Detectando el escaner...");
                //Se muestra mensaje de que se espera de respuesta de algun escaner
                lista.Items.Add("Esperando comunicación con el escaner");
                //Se guarda en el log mensaje de que se espera de respuesta de algun escaner
                log.AppendLog("Esperando comunicación con el escaner");
                try
                {
                    //Detecta los ajustes necesarios para cualquier escaner de Scantron® conectado y correctamente instalado en el sistema (Variable de SDK)
                    scanningManager.FindAndConfigureScannerAsync(ScannerSearchHints.DefaultSearchSettings, L_SCANNERMODEL_ENUM.L_SCANNERMODEL_DEFAULT);
                }
                catch (Exception ex)
                {
                    //Guarda en el log cualquier error obtenido
                    log.AppendLog(ex.Message);
                    //Muestra al usuario el estado de configuracion del escaner
                    lista.Items.Add("Setup del escaner: " + ex.Message);
                }
            }
            catch (Exception ex)
            {
                //Guarda en el log cualquier error obtenido
                log.AppendLog(ex.Message);
            }
        }

        #endregion

        #region Manejadores de los comandos de interfaz de usuario

        /// <summary>
        /// Evento que detecta la peticion de cierre por parte del usuario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Principal_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                //Detecta si existe alguna configuracion de escaneo actual
                if (_scanningManager != null)
                {
                    //Determina el estado actual del escaner y su posible liberacion
                    if (_scanningManager.Ping() != L_STATE_ENUM.L_STATE_IDLE)
                    {
                        //Muestra al usuario que se esta escaneando actualmente, es imposible el cierre
                        lbcLista.Items.Add("Se esta escaneando, imposible cerrar");
                        //Guarda en el log un intento de cierre cuando se esta escanenado
                        log.AppendLog("Intento de cierre con sesion de escaneo activa");
                        //Cancela el cierre
                        e.Cancel = true;
                    }

                    else
                    {
                        //Desecha los valores del gestor de escaneo
                        DisposeScanningManager();
                        //Guarda en el log la accion
                        log.AppendLog("Se borro el gestor de escaneo");
                        //Desecha los valores de la sesion de escaneo
                        DisposeSessionInfo();
                        //Guarda en el log la accion
                        log.AppendLog("Se borro la sesion de escaneo");
                        //Guarda en el log el cierre del programa
                        log.AppendLog("Cierre de ReadFast correcto ***********************************");
                    }
                }
            }
            catch (Exception ex)
            {
                //Guarda en el error el mensaje de cualquier error producido durante la peticion de cierre
                log.AppendLog(ex.Message);
            }
        }

        /// <summary>
        /// Evento que muestra la pantalla de ajustes a peticion del usuario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMenuAjustes_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                //Instancia el formulario a mostrar
                Ajustes mostrar = new Ajustes();
                //Guarda en el log la accion del usuario
                log.AppendLog("Se abrió la ventana ajustes");
                //Muestra la ventana de ajustes y espera hasta su cierre para continuar
                mostrar.ShowDialog();

                //Refresca los dispositivos y los inicializa
                btnMenuRefrescar_ItemClick(sender, e);
            }
            catch (Exception ex)
            {
                //Guarda en el log cualquier error producido
                log.AppendLog(ex.Message);
            }
        }

        /// <summary>
        /// Evento que inicia el proceso de escaneo a peticion del usuario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMenuIniciarEscaneo_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                //Guarda en el log la accion del usuario
                log.AppendLog("Se inicio escaneo");

                //Verifica que los ajustes necesarios no esten vacios
                if (_nucleo.LeerAjustes() != null)
                {
                    //Detecta si el escaner puede generar imagenes
                    bool imagenCompatible = _scanningManager.ActiveScanner.HardwareConfig.ImageCapable;
                    //Detecta si el escaner puede generar imagenes duales
                    bool dualCompatible = _scanningManager.ActiveScanner.HardwareConfig.DualStreamCapable;

                    //Separa las configuraciones dependiendo las funciones del escaner
                    if (!imagenCompatible || !dualCompatible)
                    {
                        //Estas instrucciones son para escaners que no pueden obtener imagenes al escanear

                        //Muestra al usuario el nombre del escaner detectado
                        lbcLista.Items.Add("El escaner: " + _scanningManager.ActiveScanner.HardwareConfig.DisplayName);
                        //Guarda en el log el nombre del escaner detectado
                        log.AppendLog("El escaner: " + _scanningManager.ActiveScanner.HardwareConfig.DisplayName);
                        //Indica al usuario y guarda en el log de que el escaner no soporta captura de imagenes
                        lbcLista.Items.Add(" no ha sido configurado para la toma de imagenes o no cuenta con dicha función.");
                        log.AppendLog("No ha sido configurado para la toma de imagenes o no cuenta con dicha función.");

                        //Si la informacion del escaner esta vacia
                        if (_scanningManager == null)
                        {
                            //Crea la informacion del escaner
                            CreateScanningManager(lbcLista);
                        }

                        //Si aun la informacion sigue vacia despues de la generacion, cancelara el escaneo
                        if (_scanningManager == null)
                        {
                            return;
                        }

                        //Si no se ha configurado correctamente el escaner en el sistema
                        if (!SetupScannerRegistry(_scanningManager))
                        {
                            //Indica al usuario de que se va a configurar el escaner para su uso
                            lbcLista.Items.Add("Necesitas configurar el escaner primero");
                            //Guarda en el log el mensaje de configuracion
                            log.AppendLog("Necesitas configurar el escaner primero");
                            //Configura el escaner para su uso y cancela el escaneo
                            SetupScanner(_scanningManager, lbcLista);
                            return;
                        }

                        else
                        {
                            //Si existe una configuracion, le muestra mensaje de que ya hay configuracion
                            lbcLista.Items.Add("Usando la configuracion previa...");
                            //Guarda en el log el mensaje
                            log.AppendLog("Usando la configuracion previa...");
                        }

                        //Detecta la comunicacion con el escaner (Si este esta en uso)
                        if (_scanningManager.Ping() != L_STATE_ENUM.L_STATE_IDLE)
                        {
                            //Guarda mensaje de que el usuario pretende escanear cuando ya existe un escaneo
                            log.AppendLog("Escaneo en curso, espere a que termine y reintente");

                            //Tira una nueva excepcion de escaneo en curso
                            throw new Exception("Escaneo en curso, espere a que termine y reintente");
                        }

                        //Crea la informacion de la sesion de escaneo
                        CreateSessionInfo();

                        //Procede a escanear segun el escaner conectado en el puerto COM
                        _scanningManager.Scan(_sessionInfo, L_PORT_ENUM.L_PORT_USE_CURRENT);
                    }

                    else
                    {
                        //Este bloque de codigo se ejecutara cuando el escaner permita la captura de imagenes al momento del escaneo

                        //Si la informacion del escaner esta vacia
                        if (_scanningManager == null)
                        {
                            //Crea la informacion del escaner
                            CreateScanningManager(lbcLista);
                        }

                        //Si aun la informacion sigue vacia despues de la generacion, cancelara el escaneo
                        if (_scanningManager == null)
                        {
                            return;
                        }

                        //Si no se ha configurado correctamente el escaner en el sistema
                        if (!SetupScannerRegistry(_scanningManager))
                        {
                            //Indica al usuario de que se va a configurar el escaner para su uso
                            lbcLista.Items.Add("Necesitas configurar el escaner primero");
                            //Guarda en el log el mensaje de configuracion
                            log.AppendLog("Necesitas configurar el escaner primero");
                            //Configura el escaner para su uso y cancela el escaneo
                            SetupScanner(_scanningManager, lbcLista);
                            return;
                        }

                        else
                        {
                            //Si existe una configuracion, le muestra mensaje de que ya hay configuracion
                            lbcLista.Items.Add("Usando la configuracion previa...");
                            //Guarda en el log el mensaje
                            log.AppendLog("Usando la configuracion previa...");
                        }

                        //Detecta la comunicacion con el escaner (Si este esta en uso)
                        if (_scanningManager.Ping() != L_STATE_ENUM.L_STATE_IDLE)
                        {
                            //Guarda mensaje de que el usuario pretende escanear cuando ya existe un escaneo
                            log.AppendLog("Escaneo en curso, espere a que termine y reintente");

                            //Tira una nueva excepcion de escaneo en curso
                            throw new Exception("Escaneo en curso, espere a que termine y reintente");
                        }

                        //Crea la informacion de la sesion de escaneo
                        CreateSessionInfo();

                        //Procede a escanear segun el escaner conectado en el puerto COM
                        _scanningManager.Scan(_sessionInfo, L_PORT_ENUM.L_PORT_USE_CURRENT);
                    }
                }

                else
                {
                    //Guarda mensaje en el log de que no ha configurado los parametros necesarios en la pantalla ajustes
                    log.AppendLog("No hay datos de ajustes, imposible continuar");
                    //Indica al usuario que antes de escear debe indicar los parametros necesarios en la pantalla ajustes
                    XtraMessageBox.Show(UserLookAndFeel.Default, "Es necesario que indique los directorios en la pantalla ajustes para empezar a escanear.", "Configure ajustes", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    //Ejecuta el evento para mostrar la pantalla de ajustes
                    btnMenuAjustes_ItemClick(sender, e);
                }
            }
            catch (Exception ex)
            {
                //Guarda en el log cualquier error producido
                log.AppendLog(ex.Message);
                //Muestra al usuario que se perdio comunicacion con el escaner
                lbcLista.Items.Add("Se perdio la comunicación con el escaner");
            }
        }

        /// <summary>
        /// Evento que muestra la pantalla de envio a peticion del usuario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMenuEnvioDatos_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                //Determina si hay informacion en el archivo de ajustes
                if (_nucleo.LeerAjustes() != null)
                {
                    //Instancia el formulario a mostrar
                    Envio mostrar = new Envio();
                    //Guarda en el log la accion
                    log.AppendLog("Se abrió la ventana envío");
                    //Muestra la ventana de envio y espera hasta su cierre para continuar
                    mostrar.ShowDialog();

                    //Refresac los dispositivos conectados para inicializarlo
                    btnMenuRefrescar_ItemClick(sender, e);
                }

                else
                {
                    //Muestra un mensaje al usuario de que hacen falta los parametros de ajustes para poder continuar
                    XtraMessageBox.Show(UserLookAndFeel.Default, "Es necesario que indique los directorios en la pantalla ajustes para envíar datos.", "Configure ajustes", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    //Muestra la ventana de ajustes para su configuracion
                    btnMenuAjustes_ItemClick(sender, e);
                }
            }
            catch (Exception ex)
            {
                //Guarda en el log cualquier error producido
                log.AppendLog(ex.Message);
            }
        }

        /// <summary>
        /// Cierra ReadFast por completo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Principal_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Refresca los dispositivos conectados, para detectar cambios de hardware a peticion del usuario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMenuRefrescar_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                //Desecha los valores del gestor de escaneo
                DisposeScanningManager();
                //Guarda en el log la accion
                log.AppendLog("Se borro el gestor de escaneo");
                //Desecha los valores de la sesion de escaneo
                DisposeSessionInfo();
                //Guarda en el log la accion
                log.AppendLog("Se borro la sesion de escaneo");

                //Se borran los mensajes que se muestran al usuario
                lbcLista.Items.Clear();

                //Inicializa las configuraciones para el escaneo
                Inicializar();
            }
            catch (Exception ex)
            {
                //Guarda cualquier error ocurrido en el log
                log.AppendLog(ex.Message);
            }
        }

        /// <summary>
        /// Indica al SO que se desea abrir un URL
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bsiSolucionesDigitales_ItemDoubleClick(object sender, ItemClickEventArgs e)
        {
            //Abre la direccion web con el programa predeterminado
            Process.Start("http://solucionesdigitales.com.mx");
            //Guarda en el log la accion realizada
            log.AppendLog("Enlazando a página web");
        }

        /// <summary>
        /// Muestra un pequeño banner, para indicar al usuario que el programa esta ocupado
        /// </summary>
        private void Ocupado()
        {
            SSMProcesando.ShowWaitForm();
        }

        /// <summary>
        /// Quita el banner que indica que el programa esta ocupado
        /// </summary>
        private void Liberar()
        {
            //Si el banner es visible, este se quita
            if (SSMProcesando.IsSplashFormVisible)
            {
                SSMProcesando.CloseWaitForm();
            }
        }

        #endregion

        #region Eventos
        
        /// <summary>
        /// Evento de funciones generadas por el SDK
        /// </summary>
        /// <param name="ScanEventArgs"></param>
        private void _scanningManager_ServerMessageBox(ScanEventArgs ScanEventArgs)
        {
            
        }

        /// <summary>
        /// Evento de funciones generadas por el SDK
        /// </summary>
        /// <param name="ScanEventArgs"></param>
        private void _scanningManager_ScannerButtonPressed(ScanEventArgs ScanEventArgs)
        {
            
        }

        /// <summary>
        /// Evento de funciones generadas por el SDK
        /// </summary>
        /// <param name="ScanEventArgs"></param>
        private void _scanningManager_ScanError(ScanEventArgs ScanEventArgs)
        {
            
        }

        /// <summary>
        /// Evento de funciones generadas por el SDK
        /// </summary>
        /// <param name="ScanEventArgs"></param>
        private void _scanningManager_PreTerminate(ScanEventArgs ScanEventArgs)
        {
            
        }

        /// <summary>
        /// Evento de funciones generadas por el SDK (Se produce cuando termina de escanear)
        /// </summary>
        /// <param name="ScanEventArgs"></param>
        private void _scanningManager_SessionComplete(ScanEventArgs ScanEventArgs)
        {
            try
            {
                //Muestra el banner de que el programa esta ocupado
                Ocupado();

                //Genera la estructura necesaria para guardarlo en un archivo JSON
                AJSON guardar = new AJSON()
                {
                    session = FolioSesion, //Obtiene el folio de la sesion
                    application = Path.GetFileName(_sessionInfo.Application), //Otiene el nombre del archivo .StxAppDef
                    profile = Path.GetFileName(_sessionInfo.ConversionProfile), //Obtiene el nombre del archivo .StxCnvPrf
                    scanner = _propiedades.escaner, //Obtiene el nombre del escaner utilizado
                    imagecapable = _propiedades.PermiteImagen,  //Obtiene si el escaner es compatible con la captura de imagenes
                    datfile = archivoDAT, //Obtiene la direccion del archivo dat
                    maindirectory = directorioPrincipal //Obtiene la direccion del directorio donde se guardan los datos
                };
                
                //Verifica que no exista el archivo con la informacion
                if (!File.Exists(directorioPrincipal + @"\ScanInfo.json"))
                {
                    //Crea el directorio que contiene el archivo con la informacion del escaneo y su estructura
                    using (File.Create(directorioPrincipal + @"\ScanInfo.json")) { }

                    //Guarda en el log la generacion del archivo
                    log.AppendLog("Se creo el archivo ScanInfo para la sesion de escaneo");
                }

                //Convierte la estructura en string
                string datos = JsonConvert.SerializeObject(guardar, Formatting.Indented);

                //Guarda los datos en el archivo json
                File.WriteAllText(directorioPrincipal + @"\ScanInfo.json", datos);
                
                //Restaura las variables utilizadas
                FolioSesion = null;

                identificador = null;

                directorioPrincipal = null;

                directorioHijo = null;

                archivoDAT = null;

                Conteo = 0;

                //Refresca los dispositivos
                btnMenuRefrescar_ItemClick(null, null);

                //Quita el banner que muestra que el programa esta ocupado
                Liberar();
            }
            catch (Exception ex)
            {
                //Guarda en el log cualquier error producido
                log.AppendLog(ex.Message);

                //Libera el
                Liberar();
            }
        }

        /// <summary>
        /// Evento de funciones generadas por el SDK
        /// </summary>
        /// <param name="ScanEventArgs"></param>
        private void _scanningManager_DocCancelled(ScanEventArgs ScanEventArgs)
        {
            
        }

        /// <summary>
        /// Evento de funciones generadas por el SDK
        /// </summary>
        /// <param name="ScanEventArgs"></param>
        private void _scanningManager_DocRejected(ScanEventArgs ScanEventArgs)
        {
            
        }

        /// <summary>
        /// Evento de funciones generadas por el SDK
        /// </summary>
        /// <param name="ScanEventArgs"></param>
        private void _scanningManager_DocStacked(ScanEventArgs ScanEventArgs)
        {
            
        }

        /// <summary>
        /// Evento de funciones generadas por el SDK
        /// </summary>
        /// <param name="ScanEventArgs"></param>
        private void _scanningManager_SheetCancelled(ScanEventArgs ScanEventArgs)
        {
            
        }

        /// <summary>
        /// Evento de funciones generadas por el SDK
        /// </summary>
        /// <param name="sea"></param>
        private void _scanningManager_SheetComplete(ScanEventArgs sea)
        {
            
        }

        /// <summary>
        /// Evento de funciones generadas por el SDK
        /// </summary>
        /// <param name="ScanEventArgs"></param>
        private void _scanningManager_SheetStacked(ScanEventArgs ScanEventArgs)
        {
            
        }

        /// <summary>
        /// Evento de funciones generadas por el SDK (Se produce cuando se pueden obtener imagenes de cada examen)
        /// </summary>
        /// <param name="ScanEventArgs"></param>
        private void _scanningManager_Clip(ScanEventArgs ScanEventArgs)
        {
            try
            {
                //Se conecta a la base de datos del SDK
                using (OleDbConnection cn = new OleDbConnection(ScanEventArgs.Document.ConnectionString))
                {
                    //Abre la conexion
                    cn.Open();

                    //Obtiene los datos de la tabla
                    OleDbCommand cmd = new OleDbCommand("SELECT ASCII FROM Master", cn);

                    //Convierte los datos a string
                    string myRecordData = cmd.ExecuteScalar().ToString();

                    //Mensaje de lectura correcta
                    lbcLista.Items.Add("Lectura correcta");
                    
                    //Recorta los valores para obtener el identificador aun cuando hay error en el mismo
                    if (myRecordData.Substring(0, 10).Contains("*"))
                    {
                        identificador = myRecordData.Substring(0, 10).Replace('*', '-');
                    }

                    else
                    {
                        identificador = myRecordData.Substring(0, 10);
                    }

                    //Aumenta el numero de serie de las hojas escaneadas
                    Conteo++;
                }
                
                //Obtiene el nombre del archivo dat
                string baseImagen = Path.GetFileNameWithoutExtension(archivoDAT);

                //Concatena el nombre que tendra la imagen a base del identificador obtenido
                string imagen = directorioHijo + @"\" + baseImagen + serie.ToString() + ".jpg";

                //Guarda el nombre que tendra la imagen en el log
                log.AppendLog("Usando directorio: " + directorioHijo + @"\" + baseImagen + serie.ToString() + ".jpg");

                //Verifica si existe la imagen... para borrarla
                if (File.Exists(imagen))
                {
                    //Borra la imagen si existe
                    File.Delete(imagen);

                    //Guarda en el log la accion de borrado
                    log.AppendLog("Se borro imagen repetida");
                }

                //Verifica si existe el directorio "Evidencias" donde se guardaran las imagenes
                if (!Directory.Exists(directorioImagenes))
                {
                    //Crea el directorio si no existe
                    Directory.CreateDirectory(directorioImagenes);
                    //Guarda en el log la accion
                    log.AppendLog("Se creo el directorio para imagenes");
                }

                //Indica que la variable sera en base a este evento (Variable de SDK)
                ClipHelper clipHelper = ScanEventArgs.ClipHelper;

                //Verifica que exista la parte superior para tomar la captura
                if (!clipHelper.TopImageExists)
                {
                    //Muestra mensaje de que existe cara del documento para escanear
                    lbcLista.Items.Add("No hay imagen para capturar");
                    //Guarda en el log el mensaje
                    log.AppendLog("No hay imagenes para escanear");
                    return;
                }
                //Muestra mensaje y guarda en el log el tipo de compresion de imagen a utilizar
                lbcLista.Items.Add("Usando compresion cpmJPEG");
                log.AppendLog("Usando compresion cpmJPEG");
                //Indica que se haran archivos con las imagenes (Variable de SDK)
                clipHelper.Archive = true;
                //Indica la profundidad de bits en color de la imagen (Variable de SDK)
                clipHelper.ClipDepth = ClipDepth.cd8BitFull;
                //Indica el nivel de compresion para la imagen (Variable de SDK)
                clipHelper.CompressionMethod = ClipCompressionMethod.cpmJPEG;
                //Indica si se debe combinar la imagen con el fondo del archivo de la plantilla (Variable de SDK)
                clipHelper.CombineWithForm = true;
                //Indica que el archivo no tendra margenes (Variable de SDK)
                clipHelper.ClipCoordinateSpace = ClipCoordinateSpace.csWorld;
                //Indica la unidad de medida con sistema ingles (Variable de SDK)
                clipHelper.UnitOfMeasure = UnitOfMeasure.uomEnglish;
                //Indica la calidad de la imagen en jpg, mejor calidad significa mayor peso, pero menor calidad indica mayor pixelacion (Pasar de 80 serian archivo pesados)
                clipHelper.JpegQuality = 75;
                
                //Graba la imagen a partir del anverso del documento y lo guarda en el directorio indicado (El anverso es marcado con _A)
                clipHelper.WriteClipToFile4(SideOfSheet.sosTop, directorioImagenes + @"\" + identificador + "_A" + ".jpg");

                //Guarda en el log  el directorio de la imagen creada
                log.AppendLog("Imagen creada: " + directorioImagenes + @"\" + identificador + "_A" + ".jpg");

                //Determina si el documento tiene datos en el reverso
                if (clipHelper.BottomImageExists)
                {
                    //Graba la imagen a partir del reverso del documento y lo guarda en el directorio indicado (El reverso es marcado con _B)
                    clipHelper.WriteClipToFile4(SideOfSheet.sosBottom, directorioImagenes + @"\" + identificador + "_B" + ".jpg");
                    //Aumenta en uno la serie de las imagenes escaneadas con imagen
                    serie++;
                    //Indica al usuario un mensaje sobre el resultado del escaneo con imagenes
                    lbcLista.Items.Add("Se han creado las imagenes correctamente");
                    //Guarda en el log  el directorio de la imagen creada
                    log.AppendLog("Imagen creada: " + directorioImagenes + @"\" + identificador + "_B" + ".jpg");
                }
                else
                {
                    //Aumenta en uno la serie de las imagenes escaneadas con imagen
                    serie++;
                    //Indica al usuario un mensaje sobre el resultado del escaneo con imagenes
                    lbcLista.Items.Add("Se ha creado la imagen correctamente");
                    //Guarda el mensaje sobre el resultado del escaneo con imagenes
                    log.AppendLog("Imagen creada correctamente");
                }
            }
            catch (Exception ex)
            {
                //Restaura el numero en serie de las imagenes
                serie = 1;
                //Guarda en el log cualquier error producido
                log.AppendLog(ex.Message);
                //Indica al usuario el error al obtener la imagen
                lbcLista.Items.Add("Es imposible capturar o guardar imagen...");
            }
        }

        /// <summary>
        /// Evento de funciones generadas por el SDK
        /// </summary>
        /// <param name="ScanEventArgs"></param>
        private void _scanningManager_XPrint(ScanEventArgs ScanEventArgs)
        {
            
        }

        /// <summary>
        /// Evento de funciones generadas por el SDK
        /// </summary>
        /// <param name="ScanEventArgs"></param>
        private void _scanningManager_PreScore(ScanEventArgs ScanEventArgs)
        {
            
        }

        /// <summary>
        /// Evento de funciones generadas por el SDK
        /// </summary>
        /// <param name="ScanEventArgs"></param>
        private void _scanningManager_Resolved(ScanEventArgs ScanEventArgs)
        {
            
        }

        /// <summary>
        /// Evento de funciones generadas por el SDK
        /// </summary>
        /// <param name="ScanEventArgs"></param>
        private void _scanningManager_FormIdentified(ScanEventArgs ScanEventArgs)
        {
            
        }

        /// <summary>
        /// Evento de funciones generadas por el SDK
        /// </summary>
        /// <param name="ScanEventArgs"></param>
        private void _scanningManager_SheetAcquired(ScanEventArgs ScanEventArgs)
        {
            
        }

        /// <summary>
        /// Evento de funciones generadas por el SDK
        /// </summary>
        /// <param name="ScanEventArgs"></param>
        private void _scanningManager_SheetInit(ScanEventArgs ScanEventArgs)
        {
            
        }

        /// <summary>
        /// Evento de funciones generadas por el SDK
        /// </summary>
        /// <param name="ScanEventArgs"></param>
        private void _scanningManager_DocInit(ScanEventArgs ScanEventArgs)
        {
            
        }

        /// <summary>
        /// Evento de funciones generadas por el SDK
        /// </summary>
        /// <param name="ScanEventArgs"></param>
        private void _scanningManager_SessionInit(ScanEventArgs ScanEventArgs)
        {
            
        }

        /// <summary>
        /// Evento de funciones generadas por el SDK
        /// </summary>
        /// <param name="sea"></param>
        private void _scanningManager_DocComplete(ScanEventArgs sea)
        {
            
        }

        /// <summary>
        /// Evento de funciones generadas por el SDK (Se produce cuando se termina la busqueda de un escaner valido conectado)
        /// </summary>
        /// <param name="sea"></param>
        private void _scanningManager_ScannerSearchComplete(ScanEventArgs sea)
        {
            try
            {
                //Muestra el resultado de la busqueda del escaner
                lbcLista.Items.Add("Búsqueda del escaner completada, resultado: " + sea.ScannerSearchResult.ToString());

                //Verifica si la configuracion es la misma que alguna previa
                if (sea.ScannerSearchResult == ScannerSearchResult.ScannerConfigured)
                {
                    //Asigna los valores de configuracion a la variable (Variable de SDK)
                    HardwareConfig hw = sea.ActiveScanner.HardwareConfig;
                    //Almacenara los valores de las configuraciones del escaner
                    string myHwConfig = null;

                    //Variables con valores del hardware del escaner (Variables de SDK)
                    BarcodePort = hw.BarcodePort;
                    BaudRate = hw.BaudRate;
                    Databits = hw.DataBits;
                    Parity = hw.Parity;
                    Stopbits = hw.StopBits;
                    Timeout = hw.Timeout;
                    WriteOperationsLog = hw.WriteOperationsLog;
                    DisplayName = hw.DisplayName;

                    myHwConfig = sea.ActiveScanner.Configuration;

                    //Verifica si el escaner puede capturar imagen
                    if (!hw.ImageCapable)
                    {
                        //Si es no es capaz de obtener imagen, se borra el directorio "Evidencias", que es innecesario para esta sesion de escaneo
                        if (Directory.Exists(directorioImagenes))
                        {
                            Directory.Delete(directorioImagenes);

                            directorioImagenes = null;
                        }
                    }

                    //Ordena los datos necesarios para el archivo JSON
                    _propiedades = new Propiedades()
                    {
                        escaner = hw.DisplayName,
                        PermiteImagen = hw.ImageCapable
                    };

                    //Guarda los ajustes en las entradas del registro
                    Registry.SetValue(@"HKEY_CURRENT_USER\SOFTWARE\Scantron\ScanTools Plus Link SDK\Common", "HwConfig", myHwConfig);
                    //Guarda en el log la accion
                    log.AppendLog("Ajustes listos, en espera de escaneo");
                    //Muestra al usuario el mensaje de que estan listos los ajustes
                    lbcLista.Items.Add("Listo, ya puede empezar a escanear");
                }

                else
                {
                    //Muestra un mensaje sobre algun error en las configuraciones o que no se a encontrado alguno
                    lbcLista.Items.Add("No ha configurado el escaner o no se encuentra correctamente conectado, corrija y refresque los dispositivos");
                    //Guarda en el log el mensaje
                    log.AppendLog("No ha configurado el escaner o no se encuentra correctamente conectado, corrija y refresque los dispositivos");
                }
            }
            catch (Exception ex)
            {
                //Guarda en el log cualquier error producido
                log.AppendLog(ex.Message);
                //Muestra el error al usuario
                lbcLista.Items.Add("Ocurrió un error al configurar el escaner, refresque los dispositivos, para tratar de corregir el error");
            }
        }

        /// <summary>
        /// Evento que se produce cuando se encuentra un escaner
        /// </summary>
        /// <param name="sea"></param>
        private void _scanningManager_ScannerFound(ScanEventArgs sea)
        {
            try
            {
                //Asigna la configuracion del hardware a la variable (Variable de SDK)
                HardwareConfig hw = sea.ActiveScanner.HardwareConfig;

                //Se asigna el puerto donde se encuentra el escaner (Variable de SDK)
                Port = sea.ActiveScanner.Port;

                //Se asigna el modelo del escaner (Variable del SDK)
                ScannerModel = hw.Model;

                //Verifica si se necesita confirmacion de ajustes por parte del usuario
                if (RequireUserToConfirmOptions)
                {
                    //Ajusta las opciones dependiendo el modelo
                    switch (hw.Model)
                    {
                        case L_SCANNERMODEL_ENUM.L_SCANNERMODEL_OPSCAN2:
                            break;
                        case L_SCANNERMODEL_ENUM.L_SCANNERMODEL_OPSCAN3:
                            break;
                        case L_SCANNERMODEL_ENUM.L_SCANNERMODEL_OPSCAN4:
                            break;
                        case L_SCANNERMODEL_ENUM.L_SCANNERMODEL_OPSCAN_INSIGHT_2:
                            break;
                        case L_SCANNERMODEL_ENUM.L_SCANNERMODEL_OPSCAN_INSIGHT_4:
                            break;

                        default:
                            HasSelectStacker = (hw.HasSelectStacker != 0);
                            DisableBarcodeReader = (hw.HasBarcodeReader == 0);
                            DisableTransportPrinter = (hw.HasTransportPrinter == 0);
                            RequireUserToConfirmOptions = false;
                            break;
                    }
                }

                //Muestra en los mensajes el nombre del escaner detectado
                lbcLista.Items.Add("Se detecto el escaner: " + hw.DisplayName);
                //Guarda en el log el nombre del escaner encontrado
                log.AppendLog("Se detecto el escaner: " + hw.DisplayName);
                //Le muestra al usuario si el escaner permite la captura de imagenes
                lbcLista.Items.Add("¿Se permite la captura de imagen?: " + (hw.ImageCapable == true ? "Sí" : "No"));
                //Guarda en el log si el escaner permite la captura de imagen
                log.AppendLog("¿Se permite la captura de imagen?: " + (hw.ImageCapable == true ? "Sí" : "No"));

                //Verifica si se necesita confirmacion de ajustes por parte del usuario
                if (RequireUserToConfirmOptions)
                {
                    //Muestra un mensaje al usuario para confirmar ajustes
                    //Actualmente se esta considerando eliminar este bloque de codigo, debido a su escaso o nulo uso
                    if (DialogResult.Yes == XtraMessageBox.Show(UserLookAndFeel.Default, "¿El escaner cuenta con un selector?", "Confirme existencia de selector", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                    {
                        HasSelectStacker = true;
                    }

                    hw.DisableSelectStacker = !HasSelectStacker ? 0 : 1;
                    hw.DisableBarcodeReader = DisableBarcodeReader ? 1 : 0;
                    hw.DisableTransportPrinter = DisableTransportPrinter ? 1 : 0;

                    string strSelectStacker = null;

                    if (hw.DisableSelectStacker == 1)
                    {
                        strSelectStacker = "Disabled";
                    }

                    else
                    {
                        strSelectStacker = "Enabled";
                    }

                    string strBarcodeReader = null;

                    if (hw.DisableBarcodeReader == 1)
                    {
                        strBarcodeReader = "Disabled";
                    }

                    else
                    {
                        strBarcodeReader = "Enabled";
                    }

                    string strTransportPrinter = null;

                    if (hw.DisableTransportPrinter == 1)
                    {
                        strTransportPrinter = "Disabled";
                    }

                    else
                    {
                        strTransportPrinter = "Enabled";
                    }

                    string SetupScannerInfo = string.Format(("¿Estan bien las opciones? " + ("\r\n" + ("\r\n" + ("Informacion del escaner:" + ('\t' + ("\r\n" + ("Familia del Escaner:" + ('\t' + ("{0}" + ('\t' + ("\r\n" + ("Bahia selectora:" + ('\t' + ("{1}" + ('\t' + ("\r\n" + ("Transporte de impresora:" + ('\t' + ("{2}" + ('\t' + ("\r\n" + ("Lector de codigos de barras:" + ('\t' + "{3}"))))))))))))))))))))))), hw.DisplayName, strSelectStacker, strTransportPrinter, strBarcodeReader);

                    if (ScannerInfoChanged)
                    {
                        if (DialogResult.No == XtraMessageBox.Show(UserLookAndFeel.Default, SetupScannerInfo, "Configuraciones del escaner", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                        {
                            sea.SkipPortOrScanner = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Guarda en el log cualquier error producido
                log.AppendLog(ex.Message);
            }
        }

        /// <summary>
        /// Evento que itera por los puertos para obtener alguno que entable comunicacion con el escaner
        /// </summary>
        /// <param name="sea"></param>
        private void _scanningManager_ScannerSearch(ScanEventArgs sea)
        {
            try
            {
                //Almacena un rango de puertos en los cuales no puede operar el escaner (Variable de SDK)
                L_PORT_ENUM[] portsToAvoid = { L_PORT_ENUM.L_PORT_OP_21_10 };
                //Intera por cada puerto a evitar
                foreach (var puerto in portsToAvoid)
                {
                    //Si el puerto es igual a algun puerto que el escaner quiere utilizar
                    if (sea.ActiveScanner.Port == puerto)
                    {
                        //Indica que este puerto se debe evitar
                        sea.SkipPortOrScanner = true;
                    }
                }
            }
            catch (Exception ex)
            {
                //Guarda en el log cualquier error producido
                log.AppendLog(ex.Message);
            }
        }

        #endregion
        
    }
}