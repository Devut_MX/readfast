﻿//Copyright © 2017-2018 Soluciones Digitales
//Todos los derechos reservados

using DevExpress.LookAndFeel;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using Logger;
using ReadFast.Controladores;
using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace ReadFast.Vistas
{
    public partial class Ajustes : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public Ajustes()
        {
            InitializeComponent();

            bsiVersion.Caption = "Versión " + _nucleo.ObtenerVersion;
        }

        //Objeto que contiene los metodos de la clase nucleo
        Nucleo _nucleo = new Nucleo();

        //Objeto que contiene los metodos de la clase Logger
        ToLog log = new ToLog();

        /// <summary>
        /// Cierra la ventana de ajustes a peticion del usuario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMenuCerrar_ItemClick(object sender, ItemClickEventArgs e)
        {
            //Cierra la ventana
            Close();
        }

        /// <summary>
        /// Muestra la pantalla para seleccionar un archivo .StxAppDef de aplicacion del escaner
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPlantillaDatos_Click(object sender, EventArgs e)
        {
            try
            {
                //Indica los parametros de la ventana de seleccion de archivo
                ofdAbrir.Title = "Seleccione la plantilla de lectura de datos a usar...";
                ofdAbrir.Filter = "Plantilla OMR (StxAppDef)|*.StxAppDef";

                //Verifica que se halla seleccionado un archivo válido
                if (ofdAbrir.ShowDialog() == DialogResult.OK)
                {
                    //Indica en la caja de texto el archivo seleccionado
                    txtArchivoPlantilla.Text = ofdAbrir.FileName;

                    //Guarda en el log el nombre del archivo seleccionado
                    log.AppendLog("Se selecciono: " + ofdAbrir.FileName);
                }
            }
            catch (Exception ex)
            {
                //Guarda en el log cualquier error producido
                log.AppendLog(ex.Message);
            }
        }

        /// <summary>
        /// Muestra una ventana para seleccionar la carpeta donde se guardaran los archivos producidos de los escaneos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDirectorioGuardadoDatos_Click(object sender, EventArgs e)
        {
            try
            {
                //Permite crear carpetas nuevas en la ventana
                fbdDirectorio.ShowNewFolderButton = true;

                //Verifica que se halla seleccionado un directorio válido
                if (fbdDirectorio.ShowDialog() == DialogResult.OK)
                {
                    //Indica en la caja de texto el directorio seleccionado
                    txtDireccionGuardadoDatos.Text = fbdDirectorio.SelectedPath;
                    //Guarda en el log el nombre del directorio seleccionado
                    log.AppendLog("Se selecciono: " + fbdDirectorio.SelectedPath);
                }
            }
            catch (Exception ex)
            {
                //Guarda en el log cualquier error producido
                log.AppendLog(ex.Message);
            }
        }

        /// <summary>
        /// Muestra la pantalla para seleccionar un archivo .StxCnvPrf de aplicacion del escaner
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPerfilDatos_Click(object sender, EventArgs e)
        {
            try
            {
                //Indica los parametros de la ventana de seleccion de archivo
                ofdAbrir.Title = "Seleccione la perfil de datos de salida a usar...";
                ofdAbrir.Filter = "Perfil de salida (StxCnvPrf)|*.StxCnvPrf";

                //Verifica que se halla seleccionado un archivo válido
                if (ofdAbrir.ShowDialog() == DialogResult.OK)
                {
                    //Indica en la caja de texto el archivo seleccionado
                    txtArchivoPerfilDatos.Text = ofdAbrir.FileName;
                    //Guarda en el log el nombre del archivo seleccionado
                    log.AppendLog("Se selecciono: " + ofdAbrir.FileName);
                }
            }
            catch (Exception ex)
            {
                //Guarda en el log cualquier error producido
                log.AppendLog(ex.Message);
            }
        }

        /// <summary>
        /// Permite guardar los ajustes a un archivo JSON
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMenuGuardarRutas_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                //Verifica que los campos necesarios esten contestados
                if (txtArchivoPerfilDatos.Text == "" || txtArchivoPlantilla.Text == "" || txtDireccionGuardadoDatos.Text == "" || txtURLWebService.Text == "")
                {
                    //Muestra un aviso al usuario de que no puede dejar vacios los campos
                    XtraMessageBox.Show(UserLookAndFeel.Default, "Las configuraciones no pueden quedar vacias, verifique por favor", "Complete los campos", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    //Se guarda la accion en el log
                    log.AppendLog("Datos necesarios vacios");

                    txtArchivoPlantilla.Focus();
                }

                else
                {
                    //Determina si el texto escrito es un url valido
                    if (!txtURLWebService.Text.Contains("http"))
                    {
                        //Muestra un mensaje de que el servidor no es un enlace web valido
                        XtraMessageBox.Show(UserLookAndFeel.Default, "Al parecer la dirección de envio de datos no es válida, ingrese una direccion web válida", "Ajustes guardados", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        //Se guarda la accion en el log
                        log.AppendLog("El url del servicio web, no es válido");

                        txtURLWebService.Focus();
                    }

                    else
                    {
                        //Pregunta al usuario si esta seguro de cambiar los ajustes
                        if (XtraMessageBox.Show(UserLookAndFeel.Default, "¿Esta seguro de confirmar los ajustes?", "Confirme guardado", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            //Guarda los ajustes en un json
                            if (_nucleo.GuardarAjustes(new Configuraciones { Aplicacion = txtArchivoPlantilla.Text, Datos = txtDireccionGuardadoDatos.Text, Perfil = txtArchivoPerfilDatos.Text, Servidor = txtURLWebService.Text }))
                            {
                                //Muestra el mensaje al usuario sobre ajustes guardados correctamente
                                XtraMessageBox.Show(UserLookAndFeel.Default, "¡Listo!, se han guardado los ajustes", "Ajustes guardados", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                //Se guarda la accion en el log
                                log.AppendLog("Ajustes guardados correctamente");

                                txtArchivoPlantilla.Focus();
                            }

                            else
                            {
                                //Muestra un mensaje de error al usuario
                                XtraMessageBox.Show(UserLookAndFeel.Default, "Ocurrio un problema al guardar los ajustes, verifica de nuevo por favor", "Ajustes no guardados", MessageBoxButtons.OK, MessageBoxIcon.Error);

                                //Se guarda la accion en el log
                                log.AppendLog("Ajustes guardados correctamente");

                                txtArchivoPlantilla.Focus();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Guarda en el log cualquier error producido
                log.AppendLog(ex.Message);
            }
        }

        /// <summary>
        /// Obtiene la informacionalmacenada en el archivo de ajustes para mostrarlas en las cajas de texto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ajustes_Shown(object sender, EventArgs e)
        {
            try
            {
                //Obtiene los ajustes del archivo json
                Configuraciones ajustes = _nucleo.LeerAjustes();

                //Si existe informacion dentro de los ajustes
                if (ajustes != null)
                {
                    txtArchivoPlantilla.Text = ajustes.Aplicacion;
                    txtDireccionGuardadoDatos.Text = ajustes.Datos;
                    txtArchivoPerfilDatos.Text = ajustes.Perfil;
                    txtURLWebService.Text = ajustes.Servidor;
                }
            }
            catch (Exception ex)
            {
                //Guarda en el log cualquier error producido
                log.AppendLog(ex.Message);
            }
        }

        /// <summary>
        /// Indica al SO que se desea abrir un URL
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bsiSolucionesDigitales_ItemDoubleClick(object sender, ItemClickEventArgs e)
        {
            //Abre la direccion web con el programa predeterminado
            Process.Start("http://solucionesdigitales.com.mx");
            //Guarda en el log la accion realizada
            log.AppendLog("Enlazando a página web");
        }
    }
}