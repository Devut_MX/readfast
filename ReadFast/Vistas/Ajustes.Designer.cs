﻿namespace ReadFast.Vistas
{
    partial class Ajustes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Ajustes));
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.btnMenuCerrar = new DevExpress.XtraBars.BarButtonItem();
            this.btnMenuAyuda = new DevExpress.XtraBars.BarButtonItem();
            this.bsiVersion = new DevExpress.XtraBars.BarStaticItem();
            this.bsiSolucionesDigitales = new DevExpress.XtraBars.BarStaticItem();
            this.btnMenuGuardarRutas = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtArchivoPlantilla = new DevExpress.XtraEditors.TextEdit();
            this.btnPlantillaDatos = new DevExpress.XtraEditors.SimpleButton();
            this.btnDirectorioGuardadoDatos = new DevExpress.XtraEditors.SimpleButton();
            this.txtDireccionGuardadoDatos = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.btnPerfilDatos = new DevExpress.XtraEditors.SimpleButton();
            this.txtArchivoPerfilDatos = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.ofdAbrir = new System.Windows.Forms.OpenFileDialog();
            this.fbdDirectorio = new System.Windows.Forms.FolderBrowserDialog();
            this.txtURLWebService = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtArchivoPlantilla.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDireccionGuardadoDatos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtArchivoPerfilDatos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtURLWebService.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.btnMenuCerrar,
            this.btnMenuAyuda,
            this.bsiVersion,
            this.bsiSolucionesDigitales,
            this.btnMenuGuardarRutas});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ribbon.MaxItemId = 6;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbon.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.MacOffice;
            this.ribbon.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.ShowOnMultiplePages;
            this.ribbon.ShowToolbarCustomizeItem = false;
            this.ribbon.Size = new System.Drawing.Size(370, 134);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            this.ribbon.Toolbar.ShowCustomizeItem = false;
            // 
            // btnMenuCerrar
            // 
            this.btnMenuCerrar.Caption = "Cerrar";
            this.btnMenuCerrar.Id = 1;
            this.btnMenuCerrar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnMenuCerrar.ImageOptions.Image")));
            this.btnMenuCerrar.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnMenuCerrar.ImageOptions.LargeImage")));
            this.btnMenuCerrar.Name = "btnMenuCerrar";
            this.btnMenuCerrar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnMenuCerrar_ItemClick);
            // 
            // btnMenuAyuda
            // 
            this.btnMenuAyuda.Caption = "Ayuda";
            this.btnMenuAyuda.Id = 2;
            this.btnMenuAyuda.ImageOptions.Image = global::ReadFast.Properties.Resources.Ayuda200;
            this.btnMenuAyuda.ImageOptions.LargeImage = global::ReadFast.Properties.Resources.Ayuda200;
            this.btnMenuAyuda.Name = "btnMenuAyuda";
            this.btnMenuAyuda.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bsiVersion
            // 
            this.bsiVersion.Caption = "Versión 1.0.0.0";
            this.bsiVersion.Id = 3;
            this.bsiVersion.Name = "bsiVersion";
            // 
            // bsiSolucionesDigitales
            // 
            this.bsiSolucionesDigitales.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bsiSolucionesDigitales.Caption = "2017 © Soluciones Digitales";
            this.bsiSolucionesDigitales.Id = 4;
            this.bsiSolucionesDigitales.Name = "bsiSolucionesDigitales";
            this.bsiSolucionesDigitales.ItemDoubleClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bsiSolucionesDigitales_ItemDoubleClick);
            // 
            // btnMenuGuardarRutas
            // 
            this.btnMenuGuardarRutas.Caption = "Guardar Ajustes";
            this.btnMenuGuardarRutas.Id = 5;
            this.btnMenuGuardarRutas.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnMenuGuardarRutas.ImageOptions.Image")));
            this.btnMenuGuardarRutas.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnMenuGuardarRutas.ImageOptions.LargeImage")));
            this.btnMenuGuardarRutas.Name = "btnMenuGuardarRutas";
            this.btnMenuGuardarRutas.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnMenuGuardarRutas_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.btnMenuCerrar);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnMenuGuardarRutas);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnMenuAyuda);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "Opciones de ajustes";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.bsiVersion);
            this.ribbonStatusBar.ItemLinks.Add(this.bsiSolucionesDigitales);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 441);
            this.ribbonStatusBar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(370, 40);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(14, 171);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(180, 16);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Archivo de la plantilla de datos:";
            // 
            // txtArchivoPlantilla
            // 
            this.txtArchivoPlantilla.Location = new System.Drawing.Point(14, 194);
            this.txtArchivoPlantilla.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtArchivoPlantilla.MenuManager = this.ribbon;
            this.txtArchivoPlantilla.Name = "txtArchivoPlantilla";
            this.txtArchivoPlantilla.Properties.ReadOnly = true;
            this.txtArchivoPlantilla.Size = new System.Drawing.Size(328, 22);
            this.txtArchivoPlantilla.TabIndex = 13;
            // 
            // btnPlantillaDatos
            // 
            this.btnPlantillaDatos.Location = new System.Drawing.Point(254, 159);
            this.btnPlantillaDatos.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPlantillaDatos.Name = "btnPlantillaDatos";
            this.btnPlantillaDatos.Size = new System.Drawing.Size(87, 28);
            this.btnPlantillaDatos.TabIndex = 14;
            this.btnPlantillaDatos.Text = "Cambiar...";
            this.btnPlantillaDatos.Click += new System.EventHandler(this.btnPlantillaDatos_Click);
            // 
            // btnDirectorioGuardadoDatos
            // 
            this.btnDirectorioGuardadoDatos.Location = new System.Drawing.Point(254, 226);
            this.btnDirectorioGuardadoDatos.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnDirectorioGuardadoDatos.Name = "btnDirectorioGuardadoDatos";
            this.btnDirectorioGuardadoDatos.Size = new System.Drawing.Size(87, 28);
            this.btnDirectorioGuardadoDatos.TabIndex = 17;
            this.btnDirectorioGuardadoDatos.Text = "Cambiar...";
            this.btnDirectorioGuardadoDatos.Click += new System.EventHandler(this.btnDirectorioGuardadoDatos_Click);
            // 
            // txtDireccionGuardadoDatos
            // 
            this.txtDireccionGuardadoDatos.Location = new System.Drawing.Point(14, 262);
            this.txtDireccionGuardadoDatos.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtDireccionGuardadoDatos.MenuManager = this.ribbon;
            this.txtDireccionGuardadoDatos.Name = "txtDireccionGuardadoDatos";
            this.txtDireccionGuardadoDatos.Properties.ReadOnly = true;
            this.txtDireccionGuardadoDatos.Size = new System.Drawing.Size(328, 22);
            this.txtDireccionGuardadoDatos.TabIndex = 16;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(14, 239);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(206, 16);
            this.labelControl7.TabIndex = 15;
            this.labelControl7.Text = "Dirección de guardado de los datos:";
            // 
            // btnPerfilDatos
            // 
            this.btnPerfilDatos.Location = new System.Drawing.Point(254, 294);
            this.btnPerfilDatos.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPerfilDatos.Name = "btnPerfilDatos";
            this.btnPerfilDatos.Size = new System.Drawing.Size(87, 28);
            this.btnPerfilDatos.TabIndex = 20;
            this.btnPerfilDatos.Text = "Cambiar...";
            this.btnPerfilDatos.Click += new System.EventHandler(this.btnPerfilDatos_Click);
            // 
            // txtArchivoPerfilDatos
            // 
            this.txtArchivoPerfilDatos.Location = new System.Drawing.Point(14, 330);
            this.txtArchivoPerfilDatos.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtArchivoPerfilDatos.MenuManager = this.ribbon;
            this.txtArchivoPerfilDatos.Name = "txtArchivoPerfilDatos";
            this.txtArchivoPerfilDatos.Properties.ReadOnly = true;
            this.txtArchivoPerfilDatos.Size = new System.Drawing.Size(328, 22);
            this.txtArchivoPerfilDatos.TabIndex = 19;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(14, 306);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(154, 16);
            this.labelControl3.TabIndex = 18;
            this.labelControl3.Text = "Archivo del perfil de datos:";
            // 
            // fbdDirectorio
            // 
            this.fbdDirectorio.Description = "Indique la carpeta donde se guardarán los datos...";
            this.fbdDirectorio.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // txtURLWebService
            // 
            this.txtURLWebService.Location = new System.Drawing.Point(14, 398);
            this.txtURLWebService.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtURLWebService.MenuManager = this.ribbon;
            this.txtURLWebService.Name = "txtURLWebService";
            this.txtURLWebService.Size = new System.Drawing.Size(328, 22);
            this.txtURLWebService.TabIndex = 24;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(14, 374);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(157, 16);
            this.labelControl1.TabIndex = 23;
            this.labelControl1.Text = "URL de recepción de datos:";
            // 
            // Ajustes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(370, 481);
            this.Controls.Add(this.txtURLWebService);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.btnPerfilDatos);
            this.Controls.Add(this.txtArchivoPerfilDatos);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.btnDirectorioGuardadoDatos);
            this.Controls.Add(this.txtDireccionGuardadoDatos);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.btnPlantillaDatos);
            this.Controls.Add(this.txtArchivoPlantilla);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Ajustes";
            this.Ribbon = this.ribbon;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "ReadFast | Ajustes";
            this.Shown += new System.EventHandler(this.Ajustes_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtArchivoPlantilla.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDireccionGuardadoDatos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtArchivoPerfilDatos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtURLWebService.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.BarButtonItem btnMenuCerrar;
        private DevExpress.XtraBars.BarButtonItem btnMenuAyuda;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraBars.BarStaticItem bsiVersion;
        private DevExpress.XtraBars.BarStaticItem bsiSolucionesDigitales;
        private DevExpress.XtraBars.BarButtonItem btnMenuGuardarRutas;
        private DevExpress.XtraEditors.TextEdit txtArchivoPlantilla;
        private DevExpress.XtraEditors.SimpleButton btnPlantillaDatos;
        private DevExpress.XtraEditors.SimpleButton btnDirectorioGuardadoDatos;
        private DevExpress.XtraEditors.TextEdit txtDireccionGuardadoDatos;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.SimpleButton btnPerfilDatos;
        private DevExpress.XtraEditors.TextEdit txtArchivoPerfilDatos;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.OpenFileDialog ofdAbrir;
        private System.Windows.Forms.FolderBrowserDialog fbdDirectorio;
        private DevExpress.XtraEditors.TextEdit txtURLWebService;
        private DevExpress.XtraEditors.LabelControl labelControl1;
    }
}