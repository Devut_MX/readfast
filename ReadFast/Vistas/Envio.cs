﻿//Copyright © 2017-2018 Soluciones Digitales
//Todos los derechos reservados

using DevExpress.LookAndFeel;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using Logger;
using Newtonsoft.Json;
using ReadFast.Controladores;
using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReadFast.Vistas
{
    public partial class Envio : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public Envio()
        {
            InitializeComponent();

            bsiVersion.Caption = "Versión " + _nucleo.ObtenerVersion;
        }

        //Objeto que contiene los metodos de la clase nucleo
        Nucleo _nucleo = new Nucleo();

        //Objeto que contiene los metodos de la clase Logger
        ToLog log = new ToLog();
        
        //Guardara la direccion del archivo con la estructura de los datos
        private string json = null;

        /// <summary>
        /// Muestra una ventana para seleccionar un archivo json con datos de escaneo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDirJSON_Click(object sender, EventArgs e)
        {
            try
            {
                //Genera una ventana que permite seleccionar un archivo json
                OpenFileDialog ofdAbrir = new OpenFileDialog();

                //Parametros de la ventana
                ofdAbrir.Title = "Seleccione archivo con la informacion a enviar...";
                ofdAbrir.Filter = "Archivo de envio (*.json)|*.json";
                ofdAbrir.InitialDirectory = Application.StartupPath + @"\Datos";
                ofdAbrir.FileName = "";

                //Verifica que se halla seleccionado un archivo valido
                if (ofdAbrir.ShowDialog() == DialogResult.OK)
                {
                    //Guarda la direccion del archivo
                    json = ofdAbrir.FileName;
                    //Muestra el nombre del archivo en la caja de texto
                    txtDirJSON.Text = Path.GetFileName(json);
                }
            }
            catch (Exception ex)
            {
                //Guarda en el log cualquier error producido
                log.AppendLog(ex.Message);
            }
        }
       
        /// <summary>
        /// Realiza el proceso de enviar los archivos al web service
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnIniciarEnvio_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                //Verifica que la direccion del archivo exista
                if (json != null)
                {
                    //Obtiene la informacion del archivo
                    string datos = File.ReadAllText(json);
                    //Convierte la informacion en una estructura manejable
                    AJSON informacion = JsonConvert.DeserializeObject<AJSON>(datos);
                    //Determina la direccion del archivo .dat
                    informacion.datfile = Path.GetDirectoryName(json) + @"\Datos\Valores.dat";
                    //Determina si la informacion de la sesion no viene vacia
                    if (string.IsNullOrWhiteSpace(informacion.session))
                    {
                        //Indica al usuario que el archivo no es valido
                        XtraMessageBox.Show(UserLookAndFeel.Default, "La información obtenida es inválida o se cambio el contenido, verifique la integridad de los datos y reintente, o seleccione otro archivo", "Datos inválidos", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                    else
                    {
                        //Verifica la integridad de la informacion de la sesion
                        if (!informacion.session.Contains("Escaneo") || informacion.session.Length < 20 || informacion.session.Length > 20)
                        {
                            //Muestra un mensaje sobre la sesion invalida
                            XtraMessageBox.Show(UserLookAndFeel.Default, "La información obtenida es inválida o se cambio el contenido, verifique la integridad de los datos y reintente, o seleccione otro archivo", "Datos inválidos", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                            btnDirJSON_Click(sender, e);
                        }

                        else
                        {
                            //Cuando proviene de i20 Plus o un escaner que permite captura de imagenes
                            if (informacion.imagecapable)
                            {
                                //Deshabilita el boton de envio
                                btnIniciarEnvio.Enabled = false;
                                //Muestra un banner de que el programa esta ocupado
                                Ocupar();
                                //Realiza el envio de las imagenes y datos de forma asincrona
                                string respuesta = await Imagenes(informacion);

                                //Determina el estado del envio
                                if (!string.IsNullOrEmpty(respuesta))
                                {
                                    //Vacia las variables
                                    txtDirJSON.Text = "";

                                    json = null;

                                    lblEstatus.Text = "---";
                                    //Quita el banner que muestra al programa ocupado
                                    Liberar();
                                    //Habilita el boton de envio nuevamente
                                    btnIniciarEnvio.Enabled = true;
                                    //Muestra un mensaje de envio exitoso
                                    XtraMessageBox.Show(UserLookAndFeel.Default, "Se han enviado los datos correctamente", "Datos enviados", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    //Guarda en el log el mensaje de envio
                                    log.AppendLog("Se han enviado los datos correctamente");
                                }

                                else
                                {
                                    //Vacia las variables
                                    lblEstatus.Text = "---";
                                    //Quita el banner que muestra al programa ocupado
                                    Liberar();
                                    //Habilita el boton de envio nuevamente
                                    btnIniciarEnvio.Enabled = true;
                                    //Guarda en el log el mensaje de envio
                                    log.AppendLog("Ocurrio un error al enviar los datos");
                                    //Muestra un mensaje de que el envio fallo y si desea intentar el envio nuevamente
                                    if (DialogResult.Yes == XtraMessageBox.Show(UserLookAndFeel.Default, "Ocurrió un error al enviar los datos, ¿Desea reintentar?", "Error al enviar los datos", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                                    {
                                        btnIniciarEnvio_ItemClick(sender, e);
                                    }
                                }
                            }

                            //Cuando proviene de EZData o algun escaner que no soporta captura de imagenes
                            else
                            {
                                //Deshabilita el boton de envio
                                btnIniciarEnvio.Enabled = false;
                                //Muestra un banner de que el programa esta ocupado
                                Ocupar();
                                //Realiza el envio de los datos de forma asincrona
                                string respuesta = await Datos(informacion);
                                //Determina el estado del envio
                                if (!string.IsNullOrEmpty(respuesta))
                                {
                                    //Vacia las variables
                                    txtDirJSON.Text = "";

                                    json = null;

                                    lblEstatus.Text = "---";
                                    //Quita el banner que muestra al programa ocupado
                                    Liberar();
                                    //Habilita el boton de envio nuevamente
                                    btnIniciarEnvio.Enabled = true;
                                    //Muestra un mensaje de envio exitoso
                                    XtraMessageBox.Show(UserLookAndFeel.Default, "Se han enviado los datos correctamente", "Datos enviados", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    //Guarda en el log el mensaje de envio
                                    log.AppendLog("Se han enviado los datos correctamente");
                                }

                                else
                                {
                                    //Vacia las variables
                                    lblEstatus.Text = "---";
                                    //Quita el banner que muestra al programa ocupado
                                    Liberar();
                                    //Habilita el boton de envio nuevamente
                                    btnIniciarEnvio.Enabled = true;
                                    //Guarda en el log el mensaje de envio
                                    log.AppendLog("Ocurrio un error al enviar los datos");
                                    //Muestra un mensaje de que el envio fallo y si desea intentar el envio nuevamente
                                    if ( DialogResult.Yes == XtraMessageBox.Show(UserLookAndFeel.Default, "Ocurrió un error al enviar los datos, ¿Desea reintentar?", "Error al enviar los datos", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                                    {
                                        btnIniciarEnvio_ItemClick(sender, e);
                                    }
                                }
                            }
                        }
                    }
                }

                else
                {
                    //Guarda en el log el error de la acción del usuario
                    log.AppendLog("No se selecciono un archivo con informacion de envio válido");
                    //Muestra un mensaje al usuario de la falta de archivo con estructura válida
                    XtraMessageBox.Show(UserLookAndFeel.Default, "Por favor, seleccione primero un archivo con la informacion de envio, antes de realizar la acción de enviar.", "Seleccione archivo con informacion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    btnDirJSON_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                //Guarda en el log cualquier error producido
                log.AppendLog(ex.Message);
                //Restaura el mensaje de estatus
                lblEstatus.Text = "---";
                //Quita el banner que muestra al programa ocupado
                Liberar();
                //Habilita el boton de envio nuevamente
                btnIniciarEnvio.Enabled = true;
            }
        }

        /// <summary>
        /// Metodo que permite el envio de los datos con imagenes
        /// </summary>
        /// <param name="informacion">Estructura que contiene la informacion de las rutas de los archivos</param>
        /// <returns>Retorna un mensaje de confirmacion si es exitoso el envio</returns>
        private Task<string> Imagenes(AJSON informacion)
        {
            try
            {
                //Se declara la variable que tendra la respuesta por parte del servidor
                string respuesta = null;
                //Se utiliza el protocolo de seguridad TLS 1.2
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                //Se obtiene la informacion del directorio que contiene las imagenes
                DirectoryInfo infoDirectorio = new DirectoryInfo(Path.GetDirectoryName(json) + @"\Datos\Evidencias");
                //Se realiza el envio de cada una de las imagenes que contiene el directorio "Evidencias"
                foreach (var imagen in infoDirectorio.GetFiles())
                {
                    //Actualiza el texto del estatus
                    lblEstatus.Text = "Enviando... " + imagen.Name;
                    //Refresca el texto
                    lblEstatus.Refresh();
                    //Crea un cliente para el envio de datos
                    using (WebClient _cliente = new WebClient())
                    {
                        //Se crean los parametros necesarios para el envio de datos al web service
                        NameValueCollection parametros = new NameValueCollection();
                        //Se indica la direccion de la imagen
                        parametros.Add("file", imagen.FullName);
                        //Se agrega la informacion solicitada en el web service en formato json
                        parametros.Add("escanerInfo", JsonConvert.SerializeObject(new Detalles() { aplicacion = informacion.application, escaner = informacion.scanner, imagen = true }, Formatting.None));
                        //Se agregan los datos al cliente
                        _cliente.QueryString = parametros;
                        //Se suben los archivos al web service mediante metodo POST
                        var responseBytes = _cliente.UploadFile(_nucleo.LeerAjustes().Servidor, "POST", imagen.FullName);
                        //Se recibe la respuesta y se asigna a la variable preparada
                        respuesta = Encoding.ASCII.GetString(responseBytes);
                    }
                }
                //Se crea un cliente para el envio del archivo dat
                using (WebClient _cliente = new WebClient())
                {
                    //Actualiza el texto del estatus
                    lblEstatus.Text = "Enviando... " + Path.GetFileName(informacion.datfile);
                    //Refresca el texto
                    lblEstatus.Refresh();

                    //Se crean los parametros necesarios para el envio de datos al web service
                    NameValueCollection parametros = new NameValueCollection();
                    //Se indica la direccion del archivo
                    parametros.Add("file", informacion.datfile);
                    //Se agrega la informacion solicitada en el web service en formato json
                    parametros.Add("escanerInfo", JsonConvert.SerializeObject(new Detalles() { aplicacion = informacion.application, escaner = informacion.scanner, imagen = false }, Formatting.None));
                    //Se agregan los datos al cliente
                    _cliente.QueryString = parametros;
                    //Se suben los archivos al web service mediante metodo POST
                    var responseBytes = _cliente.UploadFile(_nucleo.LeerAjustes().Servidor, "POST", informacion.datfile);
                    //Se recibe la respuesta y se asigna a la variable preparada
                    respuesta = Encoding.ASCII.GetString(responseBytes);
                }
                //Se guarda la respuesta del servidor en el log
                log.AppendLog("Respuesta del servidor: " + respuesta);

                return Task.FromResult(respuesta);
            }
            catch (Exception ex)
            {
                //Guarda en el log cualquier error producido
                log.AppendLog(ex.Message);
                return Task.FromResult(string.Empty);
            }
        }

        /// <summary>
        /// Metodo que permite el envio del archivo dat sin imagenes
        /// </summary>
        /// <param name="informacion">Estructura que contiene la informacion de las rutas de los archivos</param>
        /// <returns>Retorna un mensaje de confirmacion si es exitoso el envio</returns>
        private Task<string> Datos(AJSON informacion)
        {
            try
            {
                //Se declara la variable que tendra la respuesta por parte del servidor
                string respuesta = null;
                //Se utiliza el protocolo de seguridad TLS 1.2
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                //Crea un cliente para el envio de datos
                using (WebClient _cliente = new WebClient())
                {
                    //Se crean los parametros necesarios para el envio de datos al web service
                    NameValueCollection parametros = new NameValueCollection();
                    //Se indica la direccion del archivo
                    parametros.Add("file", informacion.datfile);
                    //Se agrega la informacion solicitada en el web service en formato json
                    parametros.Add("escanerInfo", JsonConvert.SerializeObject(new Detalles() { aplicacion = informacion.application, escaner = informacion.scanner, imagen = false }, Formatting.None));
                    //Se agregan los datos al cliente
                    _cliente.QueryString = parametros;
                    //Se suben los archivos al web service mediante metodo POST
                    var responseBytes = _cliente.UploadFile(_nucleo.LeerAjustes().Servidor, "POST", informacion.datfile);
                    //Se recibe la respuesta y se asigna a la variable preparada
                    respuesta = Encoding.ASCII.GetString(responseBytes);
                }
                //Se guarda la respuesta del servidor en el log
                log.AppendLog("Respuesta del servidor: " + respuesta);

                return Task.FromResult(respuesta);
            }
            catch (Exception ex)
            {
                //Guarda en el log cualquier error producido
                log.AppendLog(ex.Message);
                return Task.FromResult(string.Empty);
            }
        }
        
        /// <summary>
        /// Estructura con informacion requerida por el webservice
        /// </summary>
        public class Detalles
        {
            public string aplicacion { get; set; }
            public string escaner { get; set; }
            public bool imagen { get; set; }
        }

        /// <summary>
        /// Indica al SO que se desea abrir un URL
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bsiSolucionesDigitales_ItemDoubleClick(object sender, ItemClickEventArgs e)
        {
            //Abre la direccion web con el programa predeterminado
            Process.Start("http://solucionesdigitales.com.mx");
            //Guarda en el log la accion realizada
            log.AppendLog("Enlazando a página web");
        }

        /// <summary>
        /// Evento que se produce cuando la ventan se esta cerrando
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Envio_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                //Muestra al usuario un mensaje para que confirme el cierre
                if (DialogResult.No == XtraMessageBox.Show(UserLookAndFeel.Default, "Si cierra la ventana y esta enviando datos, estos se cancelarán.\n\n¿Desea continuar?", "Confirme cierre", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                {
                    //Sino desea salir, cancela en cierre de la ventana
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                //Guarda en el log cualquier error producido
                log.AppendLog(ex.Message);
            }
        }

        /// <summary>
        /// Muestra un pequeño banner, para indicar al usuario que el programa esta ocupado
        /// </summary>
        private void Ocupar()
        {
            SSMProcesando.ShowWaitForm();
        }

        /// <summary>
        /// Quita el banner que indica que el programa esta ocupado
        /// </summary>
        private void Liberar()
        {
            if (SSMProcesando.IsSplashFormVisible)
            {
                SSMProcesando.CloseWaitForm();
            }
        }
        
    }
}