﻿using System.Resources;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ReadFast")]
[assembly: AssemblyDescription("Software compatible con escaneres de Scantron® para la lectura de hojas por medio de OMR para recabar los datos para su procesamiento.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Soluciones Digitales")]
[assembly: AssemblyProduct("ReadFast")]
[assembly: AssemblyCopyright("Copyright © 2017")]
[assembly: AssemblyTrademark("EduManager")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("9006f149-aa49-4b8e-ba69-386d945fa738")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.1")]
[assembly: AssemblyFileVersion("1.0.0.1")]
[assembly: NeutralResourcesLanguage("es-MX")]

